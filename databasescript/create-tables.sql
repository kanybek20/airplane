CREATE TABLE public.routes
(
    route_id integer NOT NULL,
    date date NOT NULL,
    "fromAirport" integer NOT NULL,
    "toAirpot" integer NOT NULL,
    airline_id integer NOT NULL,
    PRIMARY KEY (route_id)
);
CREATE TABLE public.airlines
(
    airline_id integer NOT NULL,
    airline_name text,
    PRIMARY KEY (airline_id)
);

CREATE TABLE public.airports
(
    airport_id integer NOT NULL,
    airport_name text NOT NULL,
    city_id integer NOT NULL,
    PRIMARY KEY (airport_id)
);
CREATE TABLE public.cities
(
    city_id integer NOT NULL,
    city_name text,
    PRIMARY KEY (city_id)
);
