ALTER TABLE IF EXISTS public.routes
    ADD CONSTRAINT "fromAirport" FOREIGN KEY ("fromAirport ")
    REFERENCES public.airports (airport_id) MATCH SIMPLE
    ON UPDATE NO ACTION
    ON DELETE NO ACTION;

ALTER TABLE IF EXISTS public.routes
    ADD CONSTRAINT "toAirport" FOREIGN KEY ("toAirpot")
    REFERENCES public.airports (airport_id) MATCH SIMPLE
    ON UPDATE NO ACTION
    ON DELETE NO ACTION;

ALTER TABLE IF EXISTS public.routes
    ADD CONSTRAINT airline_id FOREIGN KEY (airline_id)
    REFERENCES public.airlines (airline_id) MATCH SIMPLE
    ON UPDATE NO ACTION
    ON DELETE NO ACTION
    NOT VALID;

ALTER TABLE IF EXISTS public.airports
    ADD CONSTRAINT city_id FOREIGN KEY (city_id)
    REFERENCES public.cities (city_id) MATCH SIMPLE
    ON UPDATE NO ACTION
    ON DELETE NO ACTION;
