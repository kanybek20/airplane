alter table airlines alter column airline_name set not null;
alter table airlines alter column airline_name type varchar(100);
alter table routes rename  column "fromAirport"  to "from_airport";
alter table routes rename  column "toAirpot"  to "to_airport";